#!/usr/bin/python3
# Based off of https://askubuntu.com/questions/464640/is-there-a-program-that-can-put-some-kind-of-visual-notification-on-my-screen-fo

import subprocess
import time
import sys

# snooze time
snooze_time = 600

def change_brightness(action):
    # Grab the contents of xrander --listactivemonitors
    screendata = subprocess.Popen(["/usr/bin/xrandr","--listactivemonitors"], stdout=subprocess.PIPE)
    
    # Iterate through the output and append the results to a monitors array
    monitors=[]
    for line in screendata.communicate()[0].decode("utf-8").split("\n"):
        if "+" in line:
            monitor = line.split(" ")
            monitors.append(monitor[-1])

    # Iterate through each monitor in monitors and adjust the brightness accordingly
    for monitor in monitors:
        if action ==  'dim':
            proc = subprocess.Popen(["/usr/bin/xrandr", "--output", monitor, "--brightness", "0.5"])
        else: 
            subprocess.Popen(["/usr/bin/xrandr", "--output", monitor, "--brightness", "1"])

# Default to snooze
snoozeoption = 1

# Kill Redshift
redshift = subprocess.Popen(["/usr/bin/pkill", "--signal", "USR1", "^redshift$"])

# Give Redshift enough time to finish stopping
# Without this the monitor dim will fail the first time
time.sleep (3)


while snoozeoption == 1:
    change_brightness('dim') 
    snoozeoption = subprocess.call(["/usr/bin/zenity","--text-info", "--html",  "--height", "500", "--width", "800", "--filename", sys.argv[1], "--ok-label=Dismiss", "--cancel-label=Snooze for 10 minutes", "--title=Reminder", "--display=:0.0"], stdout=subprocess.PIPE)
    if snoozeoption == 1:
        change_brightness('undim') 
        time.sleep(snooze_time)
    else:
        break

# Restart Redshift
subprocess.Popen(["/usr/bin/pkill", "--signal", "USR1", "^redshift$"])
